
function buildMenu()
{

    var menu = [
        ['Сюжет','index.html'],
        ['Персонажи', 'characters.html'],
        ['Язык', 'language.html'],
        ['Музыка', 'music.html'],
        ['Видео', 'video.html'],
        ['Ссылки', 'urls.html'],
        ['Связь', 'contact.html']
    ];

    var menuHandler = document.getElementById('nav');

    menu.forEach(function (item,i,menu) {
        var active = '';
        if (document.location.href.indexOf(item[1])>=0) {
            active = 'class="active"';
        }
        menuHandler.innerHTML = menuHandler.innerHTML + '<li><a '+active+' href="'+item[1]+'">'+item[0]+'</a></li>';

    });


}

buildMenu();