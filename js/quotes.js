
function getQuotes()
{

    loadQuotes('https://wau.mhn.lv/data/', function (response) {

        var quote = JSON.parse(response);

        var imageHandler = document.getElementById('character');

        var quoteHandler = document.getElementById('quote');

        imageHandler.innerHTML = '<img height="100px" src="'+quote.image+'">';

        quoteHandler.innerHTML = '<p class="quote" style="font-style: italic">"'+quote.quote+'"</p>';
    })

}

function loadQuotes(url, callback)
{

    var obj = new XMLHttpRequest();

    obj.overrideMimeType('application/json');

    obj.open('GET', url, true);

    obj.onreadystatechange = function () {

        if (obj.readyState == 4 && obj.status == 200) {

            callback(obj.responseText);

        }
    };

    obj.send(null);
}

getQuotes();

window.onload = setInterval(getQuotes, 30000);